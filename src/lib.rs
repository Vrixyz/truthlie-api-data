#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate chrono;

pub mod players;
pub mod pagination;
pub mod models;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
