#[derive(Deserialize, Debug)]
pub struct PasswordDetails {
    /// used to identify a player
    email: String,
    /// plain password
    password: String,
}

/// Strategy to be used to sign up.
#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum SignupDetails {
    /// This strategy will connect if an existing account with given password exist
    Password(PasswordDetails)
}

#[derive(Deserialize, Debug)]
pub struct SignupParameters {
    signup_details: SignupDetails,
}

#[derive(Serialize, Debug)]
pub struct SignupResponse {
    jwt: String,
}