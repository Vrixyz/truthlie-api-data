#[derive(Deserialize, Debug)]
struct AnswerDefinitionContent {
    content: String,
    is_truth: bool,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum AnswerDefinition {
    Id(i64),
    Content(AnswerDefinitionContent),
}

/**
```json
{
  "player_target_facebook_ids": [
    "12341"
  ],
  "answers": [
    {
        "content": "sentence1",
        "is_truth": true
    },
    {
        "content": "sentence2",
        "is_truth": false
    },
    {
        "id": 28
    }
  ]
}
```
**/
#[derive(Deserialize, Debug)]
struct Parameters {
    /// Players to send the card to
    player_target_facebook_ids: Vec<String>,
    answers: Vec<AnswerDefinition>,
}