use crate::models::*;

#[derive(Serialize, Debug)]
pub struct CardsGetResponse {
    offset: i64,
    count: i64,
    cards: Vec<AnswerCard3>,
    answers: Vec<Answer>,
}
// TODO: transform in :
/*
#[derive(Serialize, Debug)]
pub struct CardsGetResponse {
    cards: PaginatedResult<AnswerCard>
    answers: Vec<Answer>,
}
*/