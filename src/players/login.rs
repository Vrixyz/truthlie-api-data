#[derive(Deserialize, Debug)]
pub struct PasswordDetails {
    /// used to identify a player
    email: String,
    /// plain password
    password: String,
}

#[derive(Deserialize, Debug)]
pub struct FacebookCredentials {
    facebook_access_token: String,
}

/// Strategy to be used to authenticate the login, used to exchange for a json web token.
#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum LoginDetails {
    /// This strategy will connect if credentials are correct.
    /// - If this account does not exist, it will be created.
    /// - If this account exist, we connect to it.
    Facebook(FacebookCredentials),
    /// This strategy will connect if an existing account with given password exist
    Password(PasswordDetails)
}

#[derive(Deserialize, Debug)]
pub struct LoginParameters {
    login_details: LoginDetails,
}

#[derive(Serialize, Debug)]
pub struct LoginResponse {
    jwt: String,
}
