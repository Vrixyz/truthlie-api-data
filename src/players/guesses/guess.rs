#[derive(Serialize, Deserialize, Debug)]
struct Parameters {
    guess_id: i64,
    answer_id: i64,
}

#[derive(Deserialize, Serialize)]
struct Return {
    is_truth: bool,
}