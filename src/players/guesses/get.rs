#[derive(Serialize, Debug)]
pub struct PartialAnswer {
    answer_id: i64,
    answer_content: String,
}

#[derive(Serialize, Debug)]
pub struct DeepGuess {
    guess_id: i64,
    card_id: i64,
    sender_id: i64,
    sender_name: String,
    answers: Vec<PartialAnswer>,
}

#[derive(Serialize, Debug)]
pub struct GuessesGetResponse {
    offset: i64,
    count: i64,
    guesses: Vec<DeepGuess>,
}

#[derive(Deserialize, Debug)]
pub struct Parameters {
    // TODO: remove player_id from here: it's passed via jwt.
    player_id: i64,
    offset: i64,
    limit: i64,
    // TODO: use enum for "all_guesses", "answered_guesses" and "not_answered_guesses"
    is_answered: bool,
}