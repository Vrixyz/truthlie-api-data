#[derive(Serialize, Deserialize, Debug)]
pub struct Parameters {
    offset: i64,
    limit: i64,
}
// TODO: add PaginationResult: 
/*
#[derive(Deserialize, Debug)]
pub struct PaginationResult<T> {
    offset: i64,
    limit: i64,
    result: Vec<T>
}
*/