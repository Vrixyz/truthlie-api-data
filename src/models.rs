#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Player {
    pub id: i64,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Password {
    pub id: i64,
    pub player_id: i64,
    pub email: String,
    pub value: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthFacebook {
    pub id: i64,
    pub player_id: i64,
    pub facebook_id: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Answer {
    pub id: i64,
    pub player_id: i64,
    pub content: String,
    pub is_truth: bool,
}

impl Clone for Answer {
    fn clone(&self) -> Answer {
        Answer {
            id: self.id,
            player_id: self.player_id,
            content: self.content.clone(),
            is_truth: self.is_truth,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct NewAnswer {
    pub player_id: i64,
    pub content: String,
    pub is_truth: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AnswerCard3 {
    pub id: i64,
    pub answer1: i64,
    pub answer2: i64,
    pub answer3: i64,
    pub player_id: i64,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct AnswerGuessCard3 {
    pub id: i64,
    pub answer_card_3_id: i64,
    pub player_id: i64,
    pub answer_id: Option<i64>,
    pub creation_date: chrono::NaiveDateTime,
    pub answer_date: Option<chrono::NaiveDateTime>,
}